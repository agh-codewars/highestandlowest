import java.util.Arrays;

public class Kata {
  public static String HighAndLow(String numbers) {
        String[] numbersArray = numbers.split(" ");
        String result = String.valueOf(Arrays.stream(numbersArray).mapToInt(Integer::parseInt).max().getAsInt());
        result += " " + String.valueOf(Arrays.stream(numbersArray).mapToInt(Integer::parseInt).min().getAsInt());
        return result;
    }
}